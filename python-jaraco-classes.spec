%global pypi_name jaraco.classes

Name:           python-jaraco-classes
Version:        2.0
Release:        1%{?dist}
Summary:        Utility functions for Python class constructs
License:        MIT
URL:            https://github.com/jaraco/jaraco.classes
Source0:        https://files.pythonhosted.org/packages/source/j/%{pypi_name}/%{pypi_name}-%{version}.tar.gz
BuildArch:      noarch
BuildRequires:  python3-devel
BuildRequires:  python3-pytest
BuildRequires:  python3-setuptools
BuildRequires:  python3-setuptools_scm >= 1.15.0

%global _description\
Utility functions for Python class constructs.

%description %_description

%package -n     python3-jaraco-classes
Summary:        %{summary}
%{?python_provide:%python_provide python3-jaraco-classes}
%description -n python3-jaraco-classes %_description

%prep
%autosetup -n %{pypi_name}-%{version}

%build
SETUPTOOLS_SCM_PRETEND_VERSION=%{version}
%py3_build

%install
%py3_install

%files -n python3-jaraco-classes
%license LICENSE
%doc README.rst
%{python3_sitelib}/*

%changelog
* Mon Feb 25 2019 Ken Dreyer <kdreyer@redhat.com> - 2.0-1
- Initial package.
